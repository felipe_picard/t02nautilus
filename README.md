## README

### How to use the code:
This python code is targeted to solve project Euler's second problem, which can be found [here.] (https://projecteuler.net/problem=2)

After cloning this repository in your computer, go to the terminal and type `python3 fibonacci.py`

